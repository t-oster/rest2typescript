package de.thomas_oster.rest2typescript;

import java.io.PrintWriter;
import org.codehaus.plexus.util.StringUtils;

/**
 *  This class mimics a PrintWriter to be able to write source code
 *  with intendation without too much hassle. It's used by the TypescriptGenerator
 */
class TabWriter {
    
    protected PrintWriter w;

    public TabWriter(PrintWriter w) {
        this.w = w;
    }
    private String currentLine = "";
    int tabs = 0;

    public TabWriter print(String line) {
        currentLine += line;
        return this;
    }

    public TabWriter println(String line) {
        w.println(StringUtils.repeat(" ",2*tabs) + currentLine + line);
        currentLine = "";
        return this;
    }

    public TabWriter append(String line) {
        w.print(line);
        return this;
    }

    public TabWriter addTab() {
        tabs++;
        return this;
    }

    public TabWriter removeTab() {
        tabs--;
        return this;
    }

    public void close() {
        w.close();
    }
    
}
