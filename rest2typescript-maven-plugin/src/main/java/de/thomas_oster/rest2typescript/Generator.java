package de.thomas_oster.rest2typescript;

import de.thomas_oster.rest2typescript.annotations.ToTypescript;
import de.thomas_oster.rest2typescript.annotations.ToTypescriptExclude;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.codehaus.plexus.util.StringUtils;
import org.reflections.Reflections;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Thomas Oster (thomas.oster@upstart-it.de)
 */
public class Generator {

    private static String name2typescript(Class c) {
        return c.getSimpleName();
    }

    private static final Map<Class, String> toTypescriptType = new LinkedHashMap<>();

    static {
        toTypescriptType.put(String.class, "string");
        toTypescriptType.put(Integer.class, "number");
        toTypescriptType.put(int.class, "number");
        toTypescriptType.put(Double.class, "number");
        toTypescriptType.put(double.class, "number");
        toTypescriptType.put(Float.class, "number");
        toTypescriptType.put(float.class, "number");
        toTypescriptType.put(Short.class, "number");
        toTypescriptType.put(short.class, "number");
        toTypescriptType.put(Long.class, "number");
        toTypescriptType.put(long.class, "number");
        toTypescriptType.put(BigDecimal.class, "number");
        toTypescriptType.put(BigInteger.class, "number");
        toTypescriptType.put(Void.class, "void");
        toTypescriptType.put(void.class, "void");
        toTypescriptType.put(Boolean.class, "boolean");
        toTypescriptType.put(boolean.class, "boolean");
        toTypescriptType.put(Date.class, "string");
        toTypescriptType.put(Instant.class, "string");
        toTypescriptType.put(OffsetDateTime.class, "string");
        toTypescriptType.put(ZonedDateTime.class, "string");
        toTypescriptType.put(LocalTime.class, "string");
        toTypescriptType.put(LocalDate.class, "string");
        toTypescriptType.put(LocalDateTime.class, "string");
        
    }

    /**
     * Returns the TypeScript name of the given type. If it has to be exported
     * aswell because it's not a standard type, this method will add it to the
     * toWrite field
     *
     * @param type
     * @return
     */
    String toTypescriptType(Type gtype) {
        if (gtype instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) gtype;
            if (pType.getRawType() instanceof Class && Collection.class.isAssignableFrom((Class) pType.getRawType())) {
                return toTypescriptType(pType.getActualTypeArguments()[0]) + "[]";
            }
        } else if (gtype instanceof Class) {
            Class type = (Class) gtype;
            if (type.isArray()) {
                return toTypescriptType(type.getComponentType()) + "[]";
            }
            String result = toTypescriptType.get(type);
            if (result != null) {
                return result;
            }
            toWrite.add(type);
            return name2typescript(type);
        }
        return "any";
    }

    private final Set<String> written = new LinkedHashSet<>();
    private final Queue<Class> toWrite = new LinkedList<>();

    private Stream<Method> sorted(Method[] m) {
        return Arrays.stream(m).sorted(Comparator.comparing(Method::getName));
    }

    private static final Set<String> nullableAnnotations = new LinkedHashSet<>(); {
        nullableAnnotations.add("javax.annotation.Nullable");
        nullableAnnotations.add("jakarta.annotation.Nullable");
        nullableAnnotations.add("io.micrometer.common.lang.Nullable");
        nullableAnnotations.add("org.springframework.lang.Nullable");
    }
    
    /**
     * getAnnotation and hasAnnotation do not work... so we do string bashing
     * @return 
     */
    private boolean hasNullableAnnotation(Method m) {
        return Arrays.stream(m.getAnnotations())
                .anyMatch(a ->nullableAnnotations.contains(a.annotationType().getCanonicalName()));
    }
    
    void writeInterface(Class iface, TabWriter out) {
        String typeName = name2typescript(iface);
        if (iface.isEnum()) {
            out.println("export type " + typeName + " = " +
                Arrays.stream(iface.getEnumConstants()).map(o -> '"'+o.toString()+'"').collect(Collectors.joining(" | "))
            );
            out.println("export const " + typeName + "Constants: "+typeName+"[] = [" +
                Arrays.stream(iface.getEnumConstants()).map(o -> '"'+o.toString()+'"').collect(Collectors.joining(", "))
                +"];"
            );
            return;
        }
        out.println("export interface " + name2typescript(iface) + " {").addTab();
        Set<String> written = new LinkedHashSet<>();
        sorted(iface.getMethods()).forEach((m) -> {
            String name = m.getName();
            if (!name.equals("getClass") && name.startsWith("get")) {
                String n = StringUtils.uncapitalise(name.substring(3));
                String type = toTypescriptType(m.getGenericReturnType());
                if (written.contains(n)) {
                    return;
                }
                written.add(n);
                boolean isNullable = hasNullableAnnotation(m);
                out.println(n + (isNullable ? "?" : "" ) + ": " + type + (isNullable ? " | null " : ""));
            }
            if (name.startsWith("is") && "boolean".equals(toTypescriptType.get(m.getReturnType()))) {
                String n = StringUtils.uncapitalise(name.substring(2));
                String type = toTypescriptType(m.getGenericReturnType());
                if (written.contains(n)) {
                    return;
                }
                written.add(n);
                boolean isNullable = hasNullableAnnotation(m);
                out.println(n + (isNullable ? "?" : "" ) +": " + type + (isNullable ? " | null " : ""));
            }
        });
        out.removeTab().println("}");
    }

    private static class MappingInfo {
        String path;
        boolean isPost;
    }
    
    /**
     * returns true if the object (method or class) is annotated
     * with a @ToTypescript(false) annotation
     * @param clazz
     * @return 
     */
    public static boolean isExcludedByToTypescriptAnnotation(Class clazz) {
        for (Annotation aa : clazz.getAnnotations()) {
            try {
                if (aa.annotationType().getCanonicalName().equals("de.thomas_oster.rest2typescript.annotations.ToTypescript")
                        && !((boolean) aa.getClass().getMethod("value").invoke(aa))) {
                    return true;
                }
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    /**
     * Returns the url-path for the request-mapping annotation of this method or
     * null, if this method is not annotated with a corresponding annotation.
     *
     * @param m
     * @return
     */
    MappingInfo getRequestMappingPath(Method m) {
        if (Arrays.stream(m.getAnnotations()).anyMatch(a -> a.annotationType().getCanonicalName().equals(ToTypescriptExclude.class.getCanonicalName()))) {
            return null;
        }
        for (Annotation aa : m.getAnnotations()) {
            try {
                /**
                 * Find methods with RequestMapping and get value. Have to do
                 * this via reflection, because of different class loaders (?)
                 */
                if (aa.annotationType().getCanonicalName().equals("org.springframework.web.bind.annotation.RequestMapping")
                        || aa.annotationType().getCanonicalName().equals("org.springframework.web.bind.annotation.GetMapping")) {
                    String[] path = ((String[]) aa.getClass().getMethod("value").invoke(aa));
                    MappingInfo result = new MappingInfo();
                    result.isPost = false;
                    if (path == null || path.length == 0) {
                        result.path = m.getName();
                        return result;
                    }
                    result.path = path[0];
                    return result;
                }
                if (aa.annotationType().getCanonicalName().equals("org.springframework.web.bind.annotation.PostMapping")) {
                    String[] path = ((String[]) aa.getClass().getMethod("value").invoke(aa));
                    MappingInfo result = new MappingInfo();
                    result.isPost = true;
                    if (path == null || path.length == 0) {
                        result.path = m.getName();
                        return result;
                    }
                    result.path = path[0];
                    return result;
                }
            } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
                Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return null;
    }

    boolean hasNullableAnnotation(Parameter p) {
        for (Annotation a : p.getAnnotations()) {
            if (a.annotationType().getCanonicalName().equals("javax.annotation.Nullable")) {
                return true;
            }
        }
        return false;
    }

    void writeController(Class controller, TabWriter out) {
        //log.info("Found "+controller.getCanonicalName());
        out.println("export const " + name2typescript(controller) + " = {").addTab();
        AtomicInteger countClass = new AtomicInteger(0);
        sorted(controller.getMethods()).forEach((m) -> {
            MappingInfo mi = getRequestMappingPath(m);
            if (mi == null || mi.path == null) {
                return;
            }
            String rt = toTypescriptType(m.getGenericReturnType());
            out.print(m.getName() + ": (");
            out.print(Arrays.stream(m.getParameters())
                    .map(p -> {
                        String pt = toTypescriptType(p.getParameterizedType());
                        boolean isNullable = hasNullableAnnotation(p);
                        return p.getName() + (isNullable ? "?" : "" ) + ": " + pt + (isNullable ? " | null " : "");
                    })
                    .collect(Collectors.joining(", ")));
            out.print("): ");
            out.print("Promise<" + rt + ">");
            out.println(" => {").addTab();
            if (mi.isPost) {
                /**
                 * $.ajax({
    type: "POST",
    url: "/webservices/PodcastService.asmx/CreateMarkers",
    // The key needs to match your method's input parameter (case-sensitive).
    data: JSON.stringify({ Markers: markers }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function(data){alert(data);},
    error: function(errMsg) {
        alert(errMsg);
    }
});
                 */

                out.println("return new Promise((resolve, reject) => {").addTab().println("$.ajax({").addTab()
                    .println("type: 'POST',")
                    .println("url: '" + mi.path + "',")
                    .print("data: ");
                    if (m.getParameters().length == 0) {
                        out.println("{},");
                    }
                    else if (m.getParameters().length == 1) {
                        out.println("JSON.stringify("+m.getParameters()[0].getName()+"),");
                    }
                    out.println("contentType: 'application/json; charset=utf-8',");
                if (!"void".equals(rt) && !"string".equals(rt)) {
                    out.println("dataType: 'json',");
                }
                out.println("success: resolve,")
                    .println("error: reject")
                    .removeTab().println("})");
            }
            else {
                String method = "getJSON";
                if ("string".equals(rt)) {
                    method = "get";
                }
                out.println("return new Promise((resolve, reject) => {").addTab()
                        .print("$."+method+"('" + mi.path + "', {");
                if (m.getParameters().length > 0) {
                    out.println("").addTab().addTab();
                    AtomicInteger count = new AtomicInteger(m.getParameters().length);
                    Arrays.stream(m.getParameters())
                            .sorted(Comparator.comparing(Parameter::getName))
                            .map(p -> p.getName() + ": " + p.getName())
                            .forEach(p -> out.println(p + (count.decrementAndGet() > 0 ? "," : "")));
                    out.removeTab().removeTab();
                }
                out.println("}, resolve).fail(reject)");
            }
            out.removeTab().println("})").removeTab().println("},");
        });
        out.removeTab().println("}");
        writeRemainingTypes(out);
    }

    private void writeRemainingTypes(TabWriter out) {
        while (!toWrite.isEmpty()) {
            Class first = toWrite.poll();
            if (!written.contains(name2typescript(first))) {
                writeInterface(first, out);
                written.add(name2typescript(first));
            }
        }
    }

    public void generate(String sPackage, File target, String customHeader) throws IOException {
        generate(new Reflections(sPackage), target, customHeader);
    }

    private List<Class<?>> sorted(Set<Class<?>> o) {
        List<Class<?>> result = new LinkedList<>(o);
        result.sort((a,b) -> a.getCanonicalName().compareTo(b.getCanonicalName()));
        return result;
    }
    
    public void generate(Reflections reflections, File target, String customHeader) throws FileNotFoundException, IOException {
        TabWriter out = new TabWriter(new PrintWriter(new OutputStreamWriter(new FileOutputStream(target))));
        out.println(customHeader);

        //getLog().info("Searching @ToTypescript classes...");
        Set<Class<?>> classesWithToTypescript = reflections.getTypesAnnotatedWith(ToTypescript.class);
        classesWithToTypescript.removeIf(Generator::isExcludedByToTypescriptAnnotation);
        toWrite.addAll(sorted(classesWithToTypescript));
        writeRemainingTypes(out);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Controller.class);
        annotated.addAll(sorted(reflections.getTypesAnnotatedWith(RestController.class)));
        annotated.removeIf(Generator::isExcludedByToTypescriptAnnotation);
        for (Class<?> controller : sorted(annotated)) {
            writeController(controller, out);
        }
        out.close();
    }

    public static void main(String[] args) throws IOException {
        new Generator().generate(args[0], new File(args[1]), StringUtils.defaultString(args[2], "/// <reference path=\"node_modules/@types/jquery/index.d.ts\" />;"));
    }
}
