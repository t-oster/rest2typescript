package de.thomas_oster.rest2typescript;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import lombok.Data;
import org.junit.Test;
import static org.junit.Assert.*;

public class GeneratorTest {
    
    Generator instance = new Generator();

    @Test
    public void testtoTypescriptName() {
        assertEquals("string", instance.toTypescriptType(String.class));
        assertEquals("number", instance.toTypescriptType(int.class));
        assertEquals("number", instance.toTypescriptType(Double.class));
        assertEquals("boolean", instance.toTypescriptType(Boolean.class));
        assertEquals("boolean", instance.toTypescriptType(boolean.class));
        assertEquals("void", instance.toTypescriptType(void.class));
        assertEquals("void", instance.toTypescriptType(Void.class));
        assertEquals("string[]", instance.toTypescriptType(String[].class));
        assertEquals("ExampleClass[]", instance.toTypescriptType(ExampleClass[].class));
    }
    
    public static @Data class ExampleClass {
        String name;
        boolean male;
        List<ExampleClass> list;
    }

    private static class LineComparer extends TabWriter {
        List<String> lines = new LinkedList<>();
        String current = "";
        public LineComparer() {
            super(null);
        }

        @Override
        public TabWriter println(String line) {
            current = (current+line);
            if (current.length() > 0) {
                lines.add(current);
            }
            current = "";
            return this;
        }

        @Override
        public TabWriter print(String line) {
            current += ""+line;
            return this;
        }

        @Override
        public void close() {
            if (current.length() > 0) {
                println("");
            }
        }
        
        public void assertLines(String[] lines) {
            for (int i = 0; i < lines.length; i++) {
                if (this.lines.size() <= i) {
                    assertEquals(lines[i], "END");
                }
                assertEquals(lines[i].replaceAll("\t", "").trim(), this.lines.get(i).replaceAll("\t", "").trim());
            }
            if (this.lines.size() > lines.length) {
                fail("Additional lines beginning "+this.lines.get(lines.length));
            }
        }
        
        public void dumpLines() {
            lines.forEach(System.out::println);
        }
        
        public void assertLines(InputStream stream) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
            assertLines(rd.lines().toArray(String[]::new));
        }
        
    }
    
    @Test
    public void testWriteInterface() throws Exception {
        LineComparer w = new LineComparer();
        instance.writeInterface(ExampleClass.class, w);
        w.assertLines(new String[]{
            "export interface ExampleClass {",
                "list: ExampleClass[]",
                "name: string",
                "male: boolean",
            "}"
        });
    }

    
    
    @Test
    public void testWriteController() throws Exception {
        LineComparer w = new LineComparer();
        instance.writeController(TestController.class, w);
        w.dumpLines();
        w.assertLines(this.getClass().getResourceAsStream("testcontroller.ts"));
    }
}
