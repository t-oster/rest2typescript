export const TestController = {
  testMethod: (): Promise<string> => {
    return new Promise((resolve, reject) => {
      $.get('/bla', {}, resolve).fail(reject)
    })
  },
  testParmeterized: (test: string[]): Promise<string> => {
    return new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        url: '/blubb',
        data: JSON.stringify(test),
        contentType: 'application/json; charset=utf-8',
        success: resolve,
        error: reject
      })
    })
  },
  testVoidMethod: (): Promise<void> => {
    return new Promise((resolve, reject) => {
      $.getJSON('/testVoid', {}, resolve).fail(reject)
    })
  },
}