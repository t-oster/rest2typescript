# Changelog
## 1.3.14
- schöneres Typecript
## 1.3.13
- Fix: nullable
## 1.3.12
- Fix: dataType json
## 1.3.9
- Fix: For primitive strings, don't parse json
## 1.3
- For enums, the constants are also exported as <EnumName>Constants
## 1.2
- Use jquery only as d.ts, not as import
- Allow exporting of enums as type