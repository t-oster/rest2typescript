package de.thomas_oster.rest2typescript.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If you annotate a class with this annotation, the rest2typescript-maven-plugin
 * will export this type to the typescript file as interface (and all dependencies).
 * You can also use this with a value of false to exclude a class from
 * being exported. To exclude only ceratin methods of a rest controller, use @ToTypescriptExclude
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ToTypescript {
    boolean value() default true;
}
